﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using dotnetherkansingnick.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using dotnetherkansingnick.Abstract;
using dotnetherkansingnick.Models;

namespace dotnetherkansingnick.Controllers.Tests
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void JoinMealWithFreeSpaceTest()
        {
            //Arrange
            var MealRepo = new Mock<IMealRepository>();
            var StudentRepo = new Mock<IStudentRepository>();
            var CookStudent = new Student { Id = 1 };
            var GuestStudent = new Student { Id = 2 };
            var GuestStudent2 = new Student { Id = 3 };
            var TestMeal = new Meal { Id = 1, MaxStudentsForMeal = 1, MealCook = 1, StudentsJoined = String.Empty};

            MealRepo.Setup(x => x.SubscribeStudentToMeal(It.IsAny<int>(), It.IsAny<int>())).Returns<bool>(response => response);

            var ControllerHome = new HomeController( StudentRepo.Object, MealRepo.Object);

            //Act
            var result = MealRepo.Object.SubscribeStudentToMeal(2, 1);

            //Assert
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void JoinMealWithoutFreeSpaceTest()
        {
            //Arrange
            var MealRepo = new Mock<IMealRepository>();
            var StudentRepo = new Mock<IStudentRepository>();
            var CookStudent = new Student { Id = 1 };
            var GuestStudent = new Student { Id = 2 };
            var GuestStudent2 = new Student { Id = 3 };
            var TestMeal = new Meal { Id = 1, MaxStudentsForMeal = 1, MealCook = 1, StudentsJoined = "3" };

            MealRepo.Setup(x => x.SubscribeStudentToMeal(It.IsAny<int>(), It.IsAny<int>())).Returns<bool>(response => response);

            var ControllerHome = new HomeController(StudentRepo.Object, MealRepo.Object);

            //Act
            var result = MealRepo.Object.SubscribeStudentToMeal(2, 1);

            //Assert
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void DeleteMealWitNoPeopleSubscribed()
        {
            //Arrange
            var MealRepo = new Mock<IMealRepository>();
            var StudentRepo = new Mock<IStudentRepository>();
            var CookStudent = new Student { Id = 1 };
            var GuestStudent = new Student { Id = 2 };
            var GuestStudent2 = new Student { Id = 3 };
            var TestMeal = new Meal { Id = 1, MaxStudentsForMeal = 1, MealCook = 1, StudentsJoined = "" };

            MealRepo.Setup(x => x.DeleteMeal(It.IsAny<int>())).Returns<bool>(response => response);

            var ControllerHome = new HomeController(StudentRepo.Object, MealRepo.Object);

            //Act
            var result = MealRepo.Object.DeleteMeal(2);

            //Assert
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void DeleteMealWitPeopleSubscribed()
        {
            //Arrange
            var MealRepo = new Mock<IMealRepository>();
            var StudentRepo = new Mock<IStudentRepository>();
            var CookStudent = new Student { Id = 1 };
            var GuestStudent = new Student { Id = 2 };
            var GuestStudent2 = new Student { Id = 3 };
            var TestMeal = new Meal { Id = 1, MaxStudentsForMeal = 1, MealCook = 1, StudentsJoined = "2" };

            MealRepo.Setup(x => x.DeleteMeal(It.IsAny<int>())).Returns<bool>(response => response);

            var ControllerHome = new HomeController(StudentRepo.Object, MealRepo.Object);

            //Act
            var result = MealRepo.Object.DeleteMeal(2);

            //Assert
            Assert.AreEqual(result, false);
        }
    }
}