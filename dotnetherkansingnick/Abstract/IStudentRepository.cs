﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetherkansingnick.Models;

namespace dotnetherkansingnick.Abstract
{
    public interface IStudentRepository
    {
        void AddStudent(Student s);
        void RemoveStudent(int id);
        Student GetStudentById(int StudentId);
        Student GetStudentByEmail(string email);
        bool LoginCredentialsCorrect(string emailAdress, string password);
    }
}
