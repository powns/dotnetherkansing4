﻿using dotnetherkansingnick.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnetherkansingnick.Abstract
{
    public interface IMealRepository
    {
        void AddMeal(Meal NewMeal);
        List<Meal> GetAllMeals();
        List<Meal> GetAllMealsFromStudent(int StudentId);
        bool SubscribeStudentToMeal(int TargetStudentId, int MealId);
        bool UnSubscribeStudentToMeal(int TargetStudentId, int MealId);
        bool DeleteMeal(int MealId);
        Meal GetMealByDate(DateTime Date);
        Meal GetMealById(int MealId);
        void EditMeal(int TargetId, Meal TheNewMeal);
    }
}