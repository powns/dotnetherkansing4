﻿using dotnetherkansingnick.Abstract;
using dotnetherkansingnick.Models;
using dotnetherkansingnick.Models.ModelViews;
using Moq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;

namespace dotnetherkansingnick.Controllers
{
    public class HomeController : Controller
    {
        private IStudentRepository StudentRepository;
        private IMealRepository MealRepository;

        public Mock<IMealRepository> StudentRepositoryParam { get; internal set; }

        public HomeController(IStudentRepository StudentRepositoryParam, IMealRepository MealRepositoryParam)
        {
            StudentRepository = StudentRepositoryParam;
            MealRepository = MealRepositoryParam;
        }

        public ViewResult Index()
        {
            return View("Index", (Student)Session["LoggedIn"]);
        }

        public ActionResult ShowMealTodayPartial()
        {
            Meal MealToday = MealRepository.GetMealByDate(DateTime.Today);
            return PartialView("MealTodayPartial", MealToday);
        }

        public ViewResult MyMeals()
        {
            if ((Student)Session["LoggedIn"] != null)
            {
                Student Cook = (Student)Session["LoggedIn"];
                List<Meal> AllMealsFromCook = MealRepository.GetAllMealsFromStudent(Cook.Id);
                AllMealsFromCook AllMeals = new AllMealsFromCook { TheCook = Cook, AllMealsFromTheCook = AllMealsFromCook};
                return View(AllMeals);
            }
            else
            {
                return View("Index");
            }
        }

        [HttpGet]
        [Route("EditMeal/{MealId}")]
        public ViewResult EditMeal(int MealId)
        {
            Student CurrentUser = (Student)Session["LoggedIn"];
            Meal Target = MealRepository.GetMealById(MealId);

            if(CurrentUser != null && CurrentUser.Id == Target.MealCook && Target.StudentsJoined.Split(',').GetValue(0).Equals(""))
            {
                MealAndIdToEdit Model = new MealAndIdToEdit { TargetedMealId = Target.Id, MealToEdit = Target };
                return View(Model);
            }
            else
            {
                return View("Index");
            }
        }

        [HttpPost]
        public ViewResult EditMeal(MealAndIdToEdit EditedMealAndId)
        {
            MealRepository.EditMeal(EditedMealAndId.TargetedMealId, EditedMealAndId.MealToEdit);
            return View("Index");
        }

        [Route("JoinMeal/{MealId}")]
        public ViewResult JoinMeal(int MealId)
        {
            Student CurrentUser = (Student)Session["LoggedIn"];
            Meal Target = MealRepository.GetMealById(MealId);
            int AmountJoined = Target.StudentsJoined.Split(',').GetValue(0).Equals("") ? 0 : Target.StudentsJoined.Split(',').Length;

            if (CurrentUser != null && CurrentUser.Id != Target.MealCook && (!Target.StudentsJoined.Contains(""+CurrentUser.Id) && AmountJoined < Target.MaxStudentsForMeal))
            {
                MealRepository.SubscribeStudentToMeal(CurrentUser.Id, MealId);
            }

            return View("Index");
        }

        [Route("LeaveMeal/{MealId}")]
        public ViewResult LeaveMeal(int MealId)
        {
            Student CurrentUser = (Student)Session["LoggedIn"];
            Meal Target = MealRepository.GetMealById(MealId);

            if (CurrentUser != null && CurrentUser.Id != Target.MealCook && Target.StudentsJoined.Contains("" + CurrentUser.Id))
            {
                MealRepository.UnSubscribeStudentToMeal(CurrentUser.Id, MealId);
            }

            return View("Index");
        }

        [Route("DeleteMeal/{MealId}")]
        public ViewResult DeleteMeal(int MealId)
        {
            Student CurrentUser = (Student)Session["LoggedIn"];
            Meal Target = MealRepository.GetMealById(MealId);

            if (CurrentUser != null && CurrentUser.Id == Target.MealCook && Target.StudentsJoined.Split(',').GetValue(0).Equals(""))
            {
                MealRepository.DeleteMeal(MealId);
                return View("Index");
            }
            else
            {
                return View("Index");
            }
        }

        public ViewResult Meals()
        {
            List<Meal> AllMeals = MealRepository.GetAllMeals();

            MealsWithCook MealsWithCookList = new MealsWithCook { TheMealsWithCook = new List<MealAndCook>() };

            foreach (Meal M in AllMeals)
            {
                Student Cook = StudentRepository.GetStudentById(M.MealCook);
                MealsWithCookList.TheMealsWithCook.Add(new MealAndCook { TheMeal = M, TheCook = Cook });
            }

            AllMeals AMls = new AllMeals { Meals = AllMeals };
            return View("Meals", MealsWithCookList);
        }

        [HttpGet]
        public ViewResult Register()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Register(Student newStudent)
        {
            if (ModelState.IsValid && StudentRepository.GetStudentByEmail(newStudent.EmailAdress) == null)
            {
                Student s = newStudent;
                s.MealsSubscribedTo = new List<Meal>();
                s.MealsCookedByStudent = new List<Meal>();

                StudentRepository.AddStudent(s);

                return View("RegistrationSucess");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ViewResult Login()
        {
            if(Session["LoggedIn"] == null)
            {
                return View();
            }
            else
            {
                return View("Index");
            }
        }

        [HttpPost]
        public ViewResult Login(Student credenetials)
        {
            if (StudentRepository.LoginCredentialsCorrect(credenetials.EmailAdress, credenetials.Password) && (Student)Session["LoggedIn"] == null)
            {
                Student LoggedInStudent = StudentRepository.GetStudentByEmail(credenetials.EmailAdress);
                Session["LoggedIn"] = LoggedInStudent;

                return View("Index");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ViewResult NewMeal()
        {
            if ((Student)Session["LoggedIn"] != null)
            {
                return View();
            }
            else
            {
                return View("Index");
            }
        }

        [HttpPost]
        public ViewResult NewMeal(MealAndDateResponse response)
        {
            if (ModelState.IsValid)
            {
                CultureInfo MyCultureInfo = new CultureInfo("nl");

                DateTime MealDateTime = DateTime.Parse(response.RawDateTime, MyCultureInfo, DateTimeStyles.NoCurrentDateDefault);

                if(DateTime.Compare(MealDateTime, DateTime.Today) >= 0 && MealRepository.GetMealByDate(MealDateTime) == null)
                {
                    Student Cook = (Student)Session["LoggedIn"];
                    Meal NewMeal = new Meal
                    {
                        MealDate = MealDateTime,
                        MealName = response.MealName,
                        MealDescription = response.MealDescription,
                        MealCook = Cook.Id,
                        MaxStudentsForMeal = response.MaxStudentsForMeal,
                        MealPrice = response.MealPrice,
                        StudentsJoined = ""
                        //StudentsForMeal = new List<int>()
                    };

                    MealRepository.AddMeal(NewMeal);

                    return View("Index");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View();
            }
        }

        [Route("Meal/{MealId}")]
        public ViewResult Meal(int MealId)
        {
            Meal TargetMeal = MealRepository.GetMealById(MealId);
            List<Student> AllJoinedStudents = new List<Student>();
            Student CookFromTheMeal = StudentRepository.GetStudentById(TargetMeal.MealCook);

            if (!TargetMeal.StudentsJoined.Split(',').GetValue(0).Equals(""))
            {
                foreach (String JoinedId in TargetMeal.StudentsJoined.Split(','))
                {
                    Student Joined = StudentRepository.GetStudentById(int.Parse(JoinedId));
                    AllJoinedStudents.Add(Joined);
                }
            }

            MealAndCookAndJoined AllInfo = new MealAndCookAndJoined { TheMeal = TargetMeal, TheCook = CookFromTheMeal, JoinedStudents = AllJoinedStudents };

            return View(AllInfo);
        }

        public ViewResult LogOut()
        {
            if((Student)Session["LoggedIn"] != null)
            {
                Session["LoggedIn"] = null;
                return View();
            }
            else
            {
                return View("Index");
            }
            
        }

        public ViewResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public ViewResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        //public ViewResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
