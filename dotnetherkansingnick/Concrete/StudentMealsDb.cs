﻿using dotnetherkansingnick.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace dotnetherkansingnick.Concrete
{
    public class StudentMealsDb : DbContext
    {
        public StudentMealsDb() 
            : base("name=StudentMealsDb")
        {
            //Database.SetInitializer<StudentMealsDb>(null);
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<StudentMealsDb>());
        }

        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Meal> Meals { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }


    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}
