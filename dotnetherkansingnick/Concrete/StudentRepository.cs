﻿using dotnetherkansingnick.Abstract;
using dotnetherkansingnick.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetherkansingnick.Concrete
{
    public class StudentRepository : IStudentRepository
    {
        private StudentMealsDb context = new StudentMealsDb();

        public void AddStudent(Student s)
        {
            context.Students.Add(s);
            context.SaveChanges();
        }

        public Student GetStudentByEmail(string email)
        {
            return context.Students.SingleOrDefault(x => x.EmailAdress.Equals(email, StringComparison.InvariantCultureIgnoreCase));
        }

        public Student GetStudentById(int StudentId)
        {
            return context.Students.SingleOrDefault(x => x.Id == StudentId);
        }

        public bool LoginCredentialsCorrect(string emailAdress, string password)
        {
            return context.Students.SingleOrDefault(x => x.EmailAdress.Equals(emailAdress, StringComparison.InvariantCultureIgnoreCase) && x.Password.Equals(password)) != null;
        }

        public void RemoveStudent(int id)
        {
            throw new NotImplementedException();
        }
    }
}
