﻿using dotnetherkansingnick.Abstract;
using dotnetherkansingnick.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetherkansingnick.Concrete
{
    public class MealRepository : IMealRepository
    {
        private StudentMealsDb context = new StudentMealsDb();

        public void AddMeal(Meal NewMeal)
        {
            context.Meals.Add(NewMeal);
            context.SaveChanges();
        }

        public bool DeleteMeal(int MealId)
        {
            context.Meals.Remove(context.Meals.SingleOrDefault(x => x.Id == MealId));
            context.SaveChanges();
            return true;
        }

        public void EditMeal(int TargetId, Meal TheNewMeal)
        {
            Meal ToEdit = context.Meals.SingleOrDefault(x => x.Id == TargetId);
            ToEdit.MealName = TheNewMeal.MealName;
            ToEdit.MealDescription = TheNewMeal.MealDescription;
            ToEdit.MaxStudentsForMeal = TheNewMeal.MaxStudentsForMeal;
            if(TheNewMeal.MealPrice != 0)
            {
                ToEdit.MealPrice = TheNewMeal.MealPrice;
            }
            context.SaveChanges();
        }

        public List<Meal> GetAllMeals()
        {
            List<Meal> Meals = new List<Meal>();
            foreach(Meal Meal in context.Meals)
            {
                //If meal is today or later than today add them to the list.
                if (DateTime.Compare(Meal.MealDate, DateTime.Today) >= 0)
                {
                    Meals.Add(Meal);
                }
            }

            return Meals;
        }

        public List<Meal> GetAllMealsFromStudent(int StudentId)
        {
            List<Meal> Result = new List<Meal>();

            foreach(Meal M in context.Meals)
            {
                if(M.MealCook == StudentId)
                {
                    Result.Add(M);
                }
            }

            return Result;
        }

        public Meal GetMealByDate(DateTime Date)
        {
            return context.Meals.SingleOrDefault(
                x => x.MealDate.Day == Date.Day
                && x.MealDate.Month == Date.Month
                && x.MealDate.Year == Date.Year
            );
        }

        public Meal GetMealById(int MealId)
        {
            return context.Meals.SingleOrDefault(x => x.Id == MealId);
        }

        public bool SubscribeStudentToMeal(int TargetStudentId, int MealId)
        {
            Meal Target = context.Meals.SingleOrDefault(x => x.Id == MealId);
            if (!Target.StudentsJoined.Contains("" + TargetStudentId))
            {
                if (Target.StudentsJoined.Equals(""))
                {
                    Target.StudentsJoined = "" + TargetStudentId;
                }
                else
                {
                    Target.StudentsJoined += "," + TargetStudentId;
                }

                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UnSubscribeStudentToMeal(int TargetStudentId, int MealId)
        {
            Meal Target = context.Meals.SingleOrDefault(x => x.Id == MealId);
            if (Target.StudentsJoined.Contains("" + TargetStudentId))
            {
                
                if (Target.StudentsJoined.Split(',').Length == 1)
                {
                    Target.StudentsJoined = Target.StudentsJoined.Replace(""+TargetStudentId, "");
                }
                else
                {
                    Target.StudentsJoined = Target.StudentsJoined.Replace("," + TargetStudentId, "");
                }
                
                context.SaveChanges();
                return true;
            }
            
            return false;
        }
    }
}
