﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnetherkansingnick.Models.ModelViews
{
    public class MealAndIdToEdit
    {
        public int TargetedMealId { get; set; }
        public Meal MealToEdit { get; set; }
    }
}