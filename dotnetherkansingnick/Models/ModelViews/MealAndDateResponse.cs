﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnetherkansingnick.Models.ModelViews
{
    public class MealAndDateResponse
    {
        public string MealName { get; set; }
        public string MealDescription { get; set; }
        public int MaxStudentsForMeal { get; set; }
        public double MealPrice { get; set; }
        public string RawDateTime { get; set; }
    }
}