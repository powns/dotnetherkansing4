﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnetherkansingnick.Models.ModelViews
{
    public class AllMealsFromCook
    {
        public Student TheCook { get; set; }
        public List<Meal> AllMealsFromTheCook { get; set; }
    }
}