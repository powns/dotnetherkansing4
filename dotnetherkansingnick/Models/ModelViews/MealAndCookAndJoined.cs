﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dotnetherkansingnick.Models.ModelViews
{
    public class MealAndCookAndJoined
    {
        public Meal TheMeal { get; set; }
        public Student TheCook { get; set; }
        public List<Student> JoinedStudents { get; set; }
    }
}