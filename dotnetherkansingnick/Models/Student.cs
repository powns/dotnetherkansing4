﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace dotnetherkansingnick.Models
{
    public class Student
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter your name")]
        public string StudentName { get; set; }

        public List<Meal> MealsCookedByStudent { get; set; }
        public List<Meal> MealsSubscribedTo { get; set; }

        [Required(ErrorMessage = "Please enter your email adress")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage =  "Please enter a valid email adress")]
        public string EmailAdress { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        //TODO: Adding RegularExpression for phone number!
        public string PhoneNumber { get; set; }

        public string Password { get; set; }
    }
}