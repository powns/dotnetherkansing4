﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetherkansingnick.Models
{
    public class Meal
    {
        public int Id { get; set; }
        public DateTime MealDate { get; set; }
        public string MealName { get; set; }
        public string MealDescription { get; set; }
        public int MealCook { get; set; }
        public int MaxStudentsForMeal { get; set; }
        public double MealPrice { get; set; }
        //public virtual List<int> StudentsForMeal{ get; set; }

        public string StudentsJoined { get; set; }

        //public Meal()
        //{
        //    StudentsForMeal = new List<int>();
        //}
    }
}
